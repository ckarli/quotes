from django.conf.urls import patterns, url, include
from rest_framework import routers
import api
import views

router = routers.DefaultRouter()
router.register(r'quote', api.QuoteViewSet)


urlpatterns = [
    # urls for Django Rest Framework API
    url(r'^api/v1/', include(router.urls)),
]

urlpatterns += [
    # urls for Quote
    url(r'^debrid/quote/$', views.AccountListView.as_view(), name='quotes_quote_list'),
    url(r'^debrid/quote/create/$', views.AccountCreateView.as_view(), name='quotes_quote_create'),
    url(r'^debrid/quote/detail/(?P<id>\S+)/$', views.AccountDetailView.as_view(), name='quotes_quote_detail'),
    url(r'^debrid/quote/update/(?P<id>\S+)/$', views.AccountUpdateView.as_view(), name='quotes_quote_update'),
]

