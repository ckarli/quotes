from django import forms
from .models import Account


class QuoteForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ['username', 'password', 'expiry_date']


