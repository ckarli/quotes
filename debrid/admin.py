from django.contrib import admin
from django import forms
from .models import Account, Proxy

class AccountAdminForm(forms.ModelForm):

    class Meta:
        model = Account
        fields = '__all__'


class AccountAdmin(admin.ModelAdmin):

    form = AccountAdminForm


class ProxyAdmin(admin.ModelAdmin):


    class Meta:
        model = Proxy



admin.site.register(Account, AccountAdmin)
admin.site.register(Proxy, ProxyAdmin)


