import unittest
from django.core.urlresolvers import reverse
from django.test import Client
from .models import Quote
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_quote(**kwargs):
    defaults = {}
    defaults["text"] = "text"
    defaults["author"] = "author"
    defaults.update(**kwargs)
    return Quote.objects.create(**defaults)


class QuoteViewTest(unittest.TestCase):
    '''
    Tests for Quote
    '''
    def setUp(self):
        self.client = Client()

    def test_list_quote(self):
        url = reverse('quotes_quote_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_quote(self):
        url = reverse('quotes_quote_create')
        data = {
            "text": "text",
            "author": "author",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_quote(self):
        quote = create_quote()
        url = reverse('quotes_quote_detail', args=[quote.id,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_quote(self):
        quote = create_quote()
        data = {
            "text": "text",
            "author": "author",
        }
        url = reverse('quotes_quote_update', args=[quote.id,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


