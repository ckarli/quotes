from django.core.urlresolvers import reverse
from django.db.models import *
from django.contrib.auth.models import User
from django_extensions.db.fields import AutoSlugField
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import models as auth_models
from django.db import models as models
from django_extensions.db import fields as extension_fields


class Base(models.Model):
    created_at = models.DateField(auto_now=True)
    modified_at = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User,related_name='%(class)s__created_by')
    modified_by = models.ForeignKey(User,related_name='%(class)s__modified_at')

class Account(Base):

    # Fields
    username = CharField(max_length=50)
    password = models.CharField(max_length=50)
    expiry_date = models.DateField()

    class Meta:
        ordering = ('-id',)

    def __unicode__(self):
        return u'%s' % self.username


class Proxy(Base):
    ip = GenericIPAddressField()
    port = models.IntegerField()
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)

