import models
import serializers
from rest_framework import viewsets, permissions


class QuoteViewSet(viewsets.ModelViewSet):
    """ViewSet for the Quote class"""

    queryset = models.Account.objects.all()
    serializer_class = serializers.QuoteSerializer
    permission_classes = [permissions.IsAuthenticated]


