from django.views.generic import DetailView, ListView, UpdateView, CreateView
from .models import Account
from .forms import QuoteForm


class AccountListView(ListView):
    model = Account


class AccountCreateView(CreateView):
    model = Account
    form_class = QuoteForm


class AccountDetailView(DetailView):
    model = Account


class AccountUpdateView(UpdateView):
    model = Account
    form_class = QuoteForm

